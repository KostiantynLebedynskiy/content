<?php
	class Log {
		/**
		*	Название файла для логов
		*/
		function define_path(){
			define( "LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . '/local/logs/' . date('Y_m_d') . '-' . md5(date('Y_m_d')) . '.log' );
		}


		/**
		*	Добавляем лог
		*	Log::add('Запрос поступил.', $_POST);
		*/
		function add($text = false, $param = false){
			self::define_path();
			$date = "\r\nDate: " . date('Y-m-d H:i:s') . "\r\n";
			$request = "Request from: " . $_SERVER['REQUEST_URI'] . "\r\n";
			$host = $_SERVER['SERVER_NAME'];
			$text .= "\r\n----------------------------------------------------------";
			$text .= "\r\nHost: " . $host . $date . $request . "Data: " . "\r\n";
			//Если есть дополнительные параметры, значит тоже пишем их в лог
			if( !empty($param) ) {
				ob_start();
				var_dump($param);
				$param_to_log = ob_get_contents();
				ob_end_clean();
			} else 
			$param_to_log = '';

			self::AddMessage2Log(LOG_FILENAME, $text . $param_to_log);

		}
		function AddMessage2Log ($where = false, $data = false) {
			if($where) file_put_contents($where, $data, FILE_APPEND);
			else return false;
		}
	}