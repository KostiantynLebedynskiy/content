<?php
/* ==========================================================================
   FLAMIX | ajax отправка на почту
   ========================================================================== */
add_action('wp_ajax_fx_ajax_callback', 'fx_ajax_callback');
add_action('wp_ajax_nopriv_fx_ajax_callback', 'fx_ajax_callback');
function fx_ajax_callback(){
    $data_form_contact = $_POST['data_contact'];
    
    /*=====  Данные с формы Contact us  ======*/
    if(!is_null($data_form_contact)) { 
      $message_form = bill_template_form_1($data_form_contact['ninja_forms_field_81'], $data_form_contact['ninja_forms_field_82'], $data_form_contact['ninja_forms_field_83'],
                                          $data_form_contact['ninja_forms_field_84'],  $data_form_contact['ninja_forms_field_86'], $data_form_contact['user_id']);
      Log::add('Запрос поступил с формы Contact us', $message_form);
    }
   
    $blog_name = get_bloginfo( 'name' ); 
    $URL = $_SERVER['SERVER_NAME'];
    /*========================================================
    =   Формирования header формы Contact us для отправки   =
    ========================================================*/
    if(!is_null($data_form_contact)) {
      if(is_email($data_form_contact['ninja_forms_field_84']) && $data_form_contact['ninja_forms_field_81']) {
        $headers[] = "From: {$data_form_contact['ninja_forms_field_81']} \r\n  <info@{$URL}>\r\n";
        $headers[] = "Reply-To: {$data_form_contact['ninja_forms_field_84']}\r\n";
      }
      else $headers[] = "From: {$blog_name} <info@{$URL}>\r\n";
      $headers[] = "Content-Type: text/html; charset=UTF-8";
      $is_mailed = wp_mail( get_bloginfo('admin_email'), 'Contact us / ' . $URL, $message_form, $headers ); 
    }

    //get_bloginfo('admin_email')
    //if($is_mailed) echo 1; else echo 0;
    echo 1;
    exit(); 
}
/* ==========================================================================
   FLAMIX | форма письма для формы  Contact us
   ========================================================================== */
function bill_template_form_1( $val_1 = false, $val_2 = false, $val_3 = false, $val_4 = false, $val_5 = false, $val_6 = false){
ob_start();   ?> 
<table class="send-mail" cellpadding="0" cellspacing="0" width="100%" align="center" style="border-collapse:collapse; ">
   <tbody>

  	 <tr style="border: 1px solid #6e6e6e;"><th width="25%" align="left" style="border: 1px solid #6e6e6e;" >First Name: </th><td width="75%" style="border: 1px solid #6e6e6e;" ><?=$val_1; ?></td></tr>
    <tr style="border: 1px solid #6e6e6e;"><th width="25%" align="left" style="border: 1px solid #6e6e6e;" >Last Name: </th><td width="75%" style="border: 1px solid #6e6e6e;" ><?=$val_2; ?></td></tr>
    <tr style="border: 1px solid #6e6e6e;"><th width="25%" align="left" style="border: 1px solid #6e6e6e;" >Country: </th><td width="75%" style="border: 1px solid #6e6e6e;" ><?=$val_3; ?></td></tr>
    <tr style="border: 1px solid #6e6e6e;"><th width="25%" align="left" style="border: 1px solid #6e6e6e;" >Email: </th><td width="75%" style="border: 1px solid #6e6e6e;" ><?=$val_4; ?></td></tr>
     <tr style="border: 1px solid #6e6e6e;"><th width="25%" align="left" style="border: 1px solid #6e6e6e;" >Message: </th><td width="75%" style="border: 1px solid #6e6e6e;" ><?=$val_5; ?></td></tr>
      <tr style="border: 1px solid #6e6e6e;"><th width="25%" align="left" style="border: 1px solid #6e6e6e;" >User ID: </th><td width="75%" style="border: 1px solid #6e6e6e;" ><?php if($val_6) echo $val_6; ?></td></tr>

   </tbody>
</table>

<?php   $message = ob_get_contents(); ob_end_clean();
   return $message;
}
