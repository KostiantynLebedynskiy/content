<?php
if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
	define( 'WP_LOCAL_DEV', true );
	include( dirname( __FILE__ ) . '/local-config.php' );
} else {
	define( 'WP_LOCAL_DEV', false );
	define( 'DB_NAME', 'db_server_name' );
	define( 'DB_USER', 'user_server_name' );
	define( 'DB_PASSWORD', 'pass_server_bd' );
	define( 'DB_HOST', 'localhost' ); // Probably 'localhost'
	// ===========
	// Hide errors
	// ===========
	ini_set( 'display_errors', 0 );
	define( 'WP_DEBUG_DISPLAY', false );
	// ==============================================================
	// Salts, for security
	// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
	// ==============================================================
	define( 'AUTH_KEY',         'put your unique phrase here' );
	define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
	define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
	define( 'NONCE_KEY',        'put your unique phrase here' );
	define( 'AUTH_SALT',        'put your unique phrase here' );
	define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
	define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
	define( 'NONCE_SALT',       'put your unique phrase here' );
}
// ========================
// Custom Content Directory
// ========================
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/content' );
//Проверим, используется ли https?
//|| $_SERVER['REQUEST_SCHEME'] == 'https' || $_SERVER['SERVER_PORT'] == 443
$https =
    ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')
        || (!empty($_SERVER['HTTP_HTTPS']) && $_SERVER['HTTP_HTTPS'] != 'off')
        ) ? true : false;
        
define( 'WP_CONTENT_URL', ($https ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . '/content' );

// ========================
// Custom Content Directory
// ========================

// ================================================
// You almost certainly do not want to change these
// ================================================
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );
// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// ==============================================================
$table_prefix  = 'wp_';
// ================================
// Language
// Leave blank for American English
// ================================
define( 'WPLANG', '' );

define( 'WPCF7_AUTOP', false );
// =================================================================
// Debug mode
// Debugging? Enable these. Can also enable them in local-config.php
// =================================================================
// define( 'SAVEQUERIES', true );
// define( 'WP_DEBUG', true );
// ======================================
// Load a Memcached config if we have one
// ======================================
//if ( file_exists( dirname( __FILE__ ) . '/memcached.php' ) )
//	$memcached_servers = include( dirname( __FILE__ ) . '/memcached.php' );
// ===========================================================================================
// This can be used to programatically set the stage when deploying (e.g. production, staging)
// ===========================================================================================

/*$envs = [
  'development' => 'http://test-wp.dev',
  'staging'     => 'http://staging.test-wp.dev',
  'production'  => 'http://production-wp.dev'
];
define('ENVIRONMENTS', serialize($envs));
define('WP_ENV', 'development');*/

// ===================
// Bootstrap WordPress
// ===================
if ( !defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/wp/' );
require_once( ABSPATH . 'wp-settings.php' );

